package fr.kitsxki_.nametagapi.util;

import fr.kitsxki_.nametagapi.NameTag;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardTeam;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.UUID;
import java.util.function.Predicate;

/**
 * <a href="https://github.com/lucko/helper/blob/master/helper/src/main/java/me/lucko/helper/scoreboard/PacketScoreboardTeam.java">Useful reference</a>
 * <a href="https://wiki.vg/index.php?title=Protocol&oldid=7368#Teams">wiki.vg</a>
 */
public class NameTagUtil {

    private NameTagUtil() {
    }

    public static void initAllTags(final Player player, final Predicate<UUID> isInvisiblePredicate) {
        for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            final String onlineName = onlinePlayer.getName();

            // If the tag was already send, drop it
            sendPacket(player, destroyTeamPacket(onlineName));

            // Create a new team with the player inside
            sendPacket(player, createTeamPacket(
                    onlineName,
                    isInvisiblePredicate.test(onlinePlayer.getUniqueId()) ? "never" : "always",
                    "§f",
                    "§f",
                    onlineName
            ));
        }
    }

    public static void tag(final Player player, final Player target, final NameTag tag, final boolean invisible) {
        sendPacket(player, updateTeamPacket(
                target.getName(),
                invisible ? "never" : "always",
                tag.getPrefix(),
                tag.getSuffix(),
                tag.getColor()
        ));
    }

    public static void untag(final Player player, final Player target) {
        sendPacket(player, destroyTeamPacket(target.getName()));
    }

    private static void sendPacket(final Player player, final PacketPlayOutScoreboardTeam packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private static PacketPlayOutScoreboardTeam createTeamPacket(final String teamName,
                                                                final String nameTagVisibility,
                                                                final String prefix,
                                                                final String suffix,
                                                                final String playerName) {
        final PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
        setField(packet, "a", teamName); // ID (name)
        setField(packet, "b", teamName); // Display name
        setField(packet, "c", prefix); // Prefix
        setField(packet, "d", suffix); // Suffix
        setField(packet, "e", nameTagVisibility); // Name-tag visibility
        setField(packet, "f", 15); // White
        setField(packet, "g", Collections.singletonList(playerName));
        setField(packet, "h", 0); // 0 = Create
        return packet;
    }

    private static PacketPlayOutScoreboardTeam updateTeamPacket(final String teamName,
                                                                final String nameTagVisibility,
                                                                final String prefix,
                                                                final String suffix,
                                                                final byte color) {
        final PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
        setField(packet, "h", 2); // 2 = update
        setField(packet, "a", teamName); // ID (name)
        setField(packet, "c", prefix); // Prefix
        setField(packet, "d", suffix); // Suffix
        setField(packet, "e", nameTagVisibility); // Name-tag visibility
        setField(packet, "f", color);
        return packet;
    }

    private static PacketPlayOutScoreboardTeam destroyTeamPacket(final String teamName) {
        final PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam();
        setField(packet, "a", teamName);
        setField(packet, "h", 1); // 1 = remove
        return packet;
    }

    public static void changeInvisibleStatus(final Player player, final boolean invisible) {
        for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (onlinePlayer == null || !onlinePlayer.isOnline())
                continue;

            final PacketPlayOutScoreboardTeam packet = updateTeamPacket(
                    player.getName(),
                    invisible ? "never" : "always",
                    "",
                    "",
                    (byte) 15);
            sendPacket(player, packet);
        }
    }

    private static void setField(
            final PacketPlayOutScoreboardTeam packet,
            final String fieldName,
            final Object value
    ) {
        try {
            final Field field = packet.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(packet, value);
            field.setAccessible(false);
        } catch (final Exception exception) {
            // TODO : Throw exception
        }
    }
}
