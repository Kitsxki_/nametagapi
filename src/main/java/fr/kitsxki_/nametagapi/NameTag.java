package fr.kitsxki_.nametagapi;

public final class NameTag {

    private final String prefix;
    private final String suffix;
    private final byte color;

    public NameTag(final String prefix, final String suffix, final byte color) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.color = color;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public byte getColor() {
        return this.color;
    }

    public static final class Builder {

        private String prefix = "";
        private String suffix = "";
        private byte color = 15;

        public Builder prefix(final String prefix) {
            this.prefix = prefix;
            return this;
        }

        public Builder suffix(final String suffix) {
            this.suffix = suffix;
            return this;
        }

        public Builder color(final byte color) {
            this.color = color;
            return this;
        }

        public NameTag build() {
            return new NameTag(this.prefix, this.suffix, this.color);
        }
    }
}
