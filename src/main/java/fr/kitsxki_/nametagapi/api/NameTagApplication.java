package fr.kitsxki_.nametagapi.api;

import fr.kitsxki_.nametagapi.manager.InvisiblePlayersManager;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;
import org.bukkit.Bukkit;

public interface NameTagApplication {

    static NameTagApplication get() {
        return Bukkit.getServicesManager().load(NameTagApplication.class);
    }

    TaggedPlayerManager getTaggedPlayerManager();

    InvisiblePlayersManager getInvisiblePlayersManager();
}
