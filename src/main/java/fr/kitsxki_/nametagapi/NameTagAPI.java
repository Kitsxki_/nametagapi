package fr.kitsxki_.nametagapi;

import fr.kitsxki_.nametagapi.api.NameTagApplication;
import fr.kitsxki_.nametagapi.impl.NameTagApplicationImpl;
import fr.kitsxki_.nametagapi.listeners.ConnectionListeners;
import fr.kitsxki_.nametagapi.manager.InvisiblePlayersManager;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;

public interface NameTagAPI {

    static NameTagAPI get() {
        return Bukkit.getServicesManager().load(NameTagAPI.class);
    }

    static void register(final Plugin plugin) {
        final ServicesManager servicesManager = plugin.getServer().getServicesManager();
        servicesManager.register(NameTagAPI.class, new NameTagAPIImpl(), plugin, ServicePriority.Normal);
        servicesManager.register(NameTagApplication.class, new NameTagApplicationImpl(new TaggedPlayerManager(),
                new InvisiblePlayersManager(plugin)), plugin, ServicePriority.Normal);

        final PluginManager pluginManager = plugin.getServer().getPluginManager();
        pluginManager.registerEvents(new ConnectionListeners(), plugin);
    }

    void tag(final Player player, final Player target, final NameTag tag);

    void untag(final Player player, final Player target);

    void prefix(final Player player, final Player target, final String prefix);

    void suffix(final Player player, final Player target, final String suffix);

    void color(final Player player, final Player target, final byte color);
}
