package fr.kitsxki_.nametagapi.listeners;

import fr.kitsxki_.nametagapi.api.NameTagApplication;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListeners implements Listener {

    private final TaggedPlayerManager taggedPlayerManager;

    public ConnectionListeners() {
        this.taggedPlayerManager = NameTagApplication.get().getTaggedPlayerManager();
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (player != null && player.isOnline()) {
            this.taggedPlayerManager.createTaggedPlayer(player);
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        if (player != null && player.isOnline()) {
            this.taggedPlayerManager.deletedTaggedPlayer(player);
        }
    }
}
