package fr.kitsxki_.nametagapi;

import fr.kitsxki_.nametagapi.api.NameTagApplication;
import fr.kitsxki_.nametagapi.manager.InvisiblePlayersManager;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayer;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;
import fr.kitsxki_.nametagapi.util.NameTagUtil;
import org.bukkit.entity.Player;

public final class NameTagAPIImpl implements NameTagAPI {

    private final TaggedPlayerManager taggedPlayerManager;
    private final InvisiblePlayersManager invisiblePlayersManager;

    public NameTagAPIImpl() {
        final NameTagApplication nameTagApplication = NameTagApplication.get();
        this.taggedPlayerManager = nameTagApplication.getTaggedPlayerManager();
        this.invisiblePlayersManager = nameTagApplication.getInvisiblePlayersManager();
    }

    @Override
    public void tag(final Player player, final Player target, final NameTag tag) {
        final TaggedPlayer taggedPlayer = this.taggedPlayerManager.getTaggedPlayer(player);
        if (taggedPlayer == null) {
            return;
        }

        if (taggedPlayer.isFirstTag()) {
            NameTagUtil.initAllTags(player, this.invisiblePlayersManager::isInvisible);
        }

        taggedPlayer.tag(target.getUniqueId(), tag);
        NameTagUtil.tag(player, target, tag, invisiblePlayersManager.isInvisible(target.getUniqueId()));
    }

    @Override
    public void untag(final Player player, final Player target) {
        final TaggedPlayer taggedPlayer = this.taggedPlayerManager.getTaggedPlayer(player);
        if (taggedPlayer == null) {
            return;
        }

        if (taggedPlayer.isFirstTag()) {
            NameTagUtil.initAllTags(player, this.invisiblePlayersManager::isInvisible);
        }

        taggedPlayer.untag(target.getUniqueId());
        NameTagUtil.untag(player, target);
    }

    @Override
    public void prefix(final Player player, final Player target, final String prefix) {
        this.tag(player, target, new NameTag.Builder()
                .prefix(prefix)
                .build());
    }

    @Override
    public void suffix(final Player player, final Player target, final String suffix) {
        this.tag(player, target, new NameTag.Builder()
                .suffix(suffix)
                .build());
    }

    @Override
    public void color(final Player player, final Player target, final byte color) {
        this.tag(player, target, new NameTag.Builder()
                .color(color)
                .build());
    }
}
