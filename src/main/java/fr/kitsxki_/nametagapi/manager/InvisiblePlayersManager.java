package fr.kitsxki_.nametagapi.manager;

import fr.kitsxki_.nametagapi.api.NameTagApplication;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayer;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;
import fr.kitsxki_.nametagapi.util.NameTagUtil;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public final class InvisiblePlayersManager extends BukkitRunnable {

    private final Set<UUID> invisiblePlayers = new HashSet<>();

    private final TaggedPlayerManager taggedPlayerManager;

    public InvisiblePlayersManager(final Plugin owningPlugin) {
        this.taggedPlayerManager = NameTagApplication.get().getTaggedPlayerManager();
        this.runTaskTimer(owningPlugin, 0L, 1L);
    }

    @Override
    public void run() {
        for (final TaggedPlayer taggedPlayer : this.taggedPlayerManager.getOnlineTaggedPlayers()) {
            final Player player = taggedPlayer.getBukkitPlayer();
            // Has become invisible
            if (player.hasPotionEffect(PotionEffectType.INVISIBILITY) && this.invisiblePlayers.add(taggedPlayer.getUniqueId())) {
                NameTagUtil.changeInvisibleStatus(player, true);
            }

            // Is no longer invisible
            else if (!player.hasPotionEffect(PotionEffectType.INVISIBILITY) && this.invisiblePlayers.remove(taggedPlayer.getUniqueId())) {
                NameTagUtil.changeInvisibleStatus(player, false);
            }
        }
    }

    public boolean isInvisible(final UUID uuid) {
        return this.invisiblePlayers.contains(uuid);
    }
}
