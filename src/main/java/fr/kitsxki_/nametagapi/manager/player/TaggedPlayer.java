package fr.kitsxki_.nametagapi.manager.player;

import fr.kitsxki_.nametagapi.NameTag;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class TaggedPlayer {

    // Player's unique id
    private final UUID uniqueId;

    // Tags applied on each player
    private final Map<UUID, NameTag> tags = new HashMap<>();
    private boolean firstTag;

    public TaggedPlayer(final UUID uniqueId) {
        this.uniqueId = uniqueId;

        this.firstTag = true;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(this.uniqueId);
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public Map<UUID, NameTag> getTags() {
        return this.tags;
    }

    public NameTag getTag(final UUID uuid) {
        return this.tags.getOrDefault(uuid, null);
    }

    public void tag(final UUID uniqueId, final NameTag tag) {
        this.tags.put(uniqueId, tag);
    }

    public void untag(final UUID uniqueId) {
        this.tags.remove(uniqueId);
    }

    public boolean isFirstTag() {
        return this.firstTag;
    }

    public void setFirstTag(final boolean firstTag) {
        this.firstTag = firstTag;
    }
}
