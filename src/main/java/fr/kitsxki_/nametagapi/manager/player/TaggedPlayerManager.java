package fr.kitsxki_.nametagapi.manager.player;

import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public final class TaggedPlayerManager {

    private final Map<UUID, TaggedPlayer> taggedPlayers = new HashMap<>();

    public TaggedPlayerManager() {
    }

    public void createTaggedPlayer(final Player player) {
        final UUID uniqueId = player.getUniqueId();
        this.taggedPlayers.put(uniqueId, new TaggedPlayer(uniqueId));
    }

    public void deletedTaggedPlayer(final Player player) {
        final UUID uniqueId = player.getUniqueId();
        this.taggedPlayers.remove(uniqueId);
    }

    public TaggedPlayer getTaggedPlayer(final Player player) {
        return this.taggedPlayers.getOrDefault(player.getUniqueId(), null);
    }

    public Map<UUID, TaggedPlayer> getTaggedPlayers() {
        return this.taggedPlayers;
    }

    public List<TaggedPlayer> getAllTaggedPlayers() {
        return new ArrayList<>(this.taggedPlayers.values());
    }

    public List<TaggedPlayer> getOnlineTaggedPlayers() {
        return this.getAllTaggedPlayers().stream()
                .filter(tp -> tp.getBukkitPlayer() != null && tp.getBukkitPlayer().isOnline())
                .collect(Collectors.toList());
    }
}
