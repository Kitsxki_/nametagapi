package fr.kitsxki_.nametagapi.impl;

import fr.kitsxki_.nametagapi.api.NameTagApplication;
import fr.kitsxki_.nametagapi.manager.InvisiblePlayersManager;
import fr.kitsxki_.nametagapi.manager.player.TaggedPlayerManager;

public final class NameTagApplicationImpl implements NameTagApplication {

    private final TaggedPlayerManager taggedPlayerManager;
    private final InvisiblePlayersManager invisiblePlayersManager;

    public NameTagApplicationImpl(final TaggedPlayerManager taggedPlayerManager, final InvisiblePlayersManager invisiblePlayersManager) {
        this.taggedPlayerManager = taggedPlayerManager;
        this.invisiblePlayersManager = invisiblePlayersManager;
    }

    @Override
    public TaggedPlayerManager getTaggedPlayerManager() {
        return this.taggedPlayerManager;
    }

    @Override
    public InvisiblePlayersManager getInvisiblePlayersManager() {
        return this.invisiblePlayersManager;
    }
}
